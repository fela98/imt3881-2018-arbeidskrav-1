import matplotlib.pyplot as plt
import numpy as np

temperatures = np.genfromtxt("temperature.csv", dtype=float)


def generate_daily_chart():
    plt.xlabel("Dager etter 1. januar 2017")
    plt.ylabel("Grader Celsius")
    generate_chart(temperatures, "daily.png")


def generate_daily_diff_chart():
    plt.xlabel("Dager etter 1. januar 2017")
    plt.ylabel("Differanse max-min grader Celsius")
    generate_chart(diff_max_min(temperatures), "daily_diff.png")


def generate_weekly_chart():
    plt.xlabel("Uker etter 1. januar 2017")
    plt.ylabel("Grader Celsius")
    generate_chart(get_weekly_average(temperatures), "weekly.png")


def generate_weekly_diff_chart():
    plt.xlabel("Uker etter 1. januar 2017")
    plt.ylabel("Differanse max-min grader Celsius")
    generate_chart(diff_max_min(get_weekly_average(temperatures)), "weekly_diff.png")


def diff_max_min(data):
    return data[:, 0] - data[:, 1]


def get_weekly_average(data):
    return np.mean(data.reshape(-1, 7, 3), axis=1)


def generate_chart(data, filename):
    if len(data.shape) == 2:
        for i, name in enumerate(["max", "min", "avg"]):
            print_details_and_plot(data[:, i], name)
    else:
        print_details_and_plot(data)

    plt.savefig(filename)
    plt.clf()


def print_details_and_plot(data, name=""):
    if len(name) != 0:
        print(name + ":")

    print("Average: " + str(np.mean(data)))
    print("Standard deviation: " + str(np.std(data)))
    plt.plot(data)

print("----- DAILY -----")
generate_daily_chart()
print("----- DAILY DIFF -----")
generate_daily_diff_chart()
print("----- WEEKLY -----")
generate_weekly_chart()
print("----- WEEKLY DIFF -----")
generate_weekly_diff_chart()
